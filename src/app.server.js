import React from 'react'
import { renderToString } from 'react-dom/server'
import { match, RouterContext } from 'react-router'
import routes from './routes'
import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

var path = require('path');
var express = require('express');
var cookieParser = require('cookie-parser');
var app = express();

app.use(cookieParser());

app.use('/public', express.static(path.join(__dirname, '../', 'dist')));

app.get('*', (req, res) => {
    match({ routes: routes, location: req.url }, (err, redirect, props) => {
        if (err) {
            res.status(500).send(err.message)
        } else if (redirect) {
            res.redirect(redirect.pathname + redirect.search)
        } else if (props) {

            var pageVersion = getPageVersion(req,res);
            var initialState = {home: {pageVersion: pageVersion}};
            const store = configureStore(initialState);

            const appHtml = renderToString(
                <Provider store={store}>
                    {  <RouterContext {...props}/> }
                </Provider>
            );

            res.send(renderPage(appHtml))
        } else {
            res.status(404).send('Not Found')
        }
    })
})

function renderPage(appHtml) {
    return `
    <!doctype html>
    <html>
    <meta charset=utf-8/>
    <title>React</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <div id="app">${appHtml}</div>
   `;
    //<script src="/public/bundle.js"></script>
}

function getPageVersion(req, res){
    if(req.cookies.pageVersion){
        return req.cookies.pageVersion
    }
    var pageVersion;
    if(Math.round(Math.random()) == 0){
        pageVersion = 'A';
    }else{
        pageVersion = 'B';
    }
    res.cookie('pageVersion' , pageVersion, {maxAge: 365 * 24 * 60 * 60 * 10000});
    return  pageVersion;
}

var PORT = process.env.PORT || 3000
app.listen(PORT, function() {
    console.log('Production Express server running at localhost:' + PORT)
});