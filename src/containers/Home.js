import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as pageActions from '../actions/HomeActions'

@connect(state => ({ home: state.home }), dispatch =>({pageActions: bindActionCreators(pageActions, dispatch)}))

class Home extends Component {

    componentDidMount(){

    }

    render() {
        let version  = this.props.home.pageVersion;
        let strokeColor = version == 'A' ? 'green' : 'yellow';
        return <div>
            <div>Page version: {version}</div>
            <div style={{marginTop: 100}}>
                <svg height="500" width="500">
                    <line x1="0" y1="0" x2="500" y2="250" style={{stroke:strokeColor,strokeWidth:2}} />
                </svg>
            </div>

        </div>
    }
}

export default Home;
