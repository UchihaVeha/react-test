import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppBar from 'material-ui/lib/app-bar';
import FlatButton from 'material-ui/lib/flat-button';
import { Link } from 'react-router'

@connect(state => ({ home: state.home }))

class App extends Component {
    render() {

        return <div>
            <AppBar title={<span>Home</span>} />

            <div className="content">
                {this.props.children}
            </div>
        </div>
    }
}
export default App;