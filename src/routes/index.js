import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from '../containers/App'
import Home from '../containers/Home'


module.exports = (
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
    </Route>
)